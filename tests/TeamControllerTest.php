<?php

use App\Models\Team;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;


class TeamControllerTest extends TestCase
{

    //Check index function. Show all teams
    public function testIndexTeam()
    {
        $response = $this -> get('/api/teams');
        $response->seeJsonStructure(['0']);
        $response->assertResponseStatus(200);
    }

    //Check showTeam function
    public function testShowTeam()
    {
        $team = Team::factory()->create();

        $response = $this->get('/api/teams/' . $team->id, []);
        $response->assertResponseStatus(200);
    }

    //Check showTeam function when there is no specified team
    public function testShowTeamFalse()
    {
        $response = $this->get('/api/teams/10000000000000', []);
        $response->assertResponseStatus(404);
    }

    //Check storeTeam function
    public function testStoreTeam()
    {
        //Method 1

        $response = $this->post('/api/teams', [
            'title' => 'Team_example'
        ]);

        $response ->assertTrue(true);

        //Method 2

        $team = Team::factory()->create();

        $this->json('post', 
            "/api/teams", $team->toArray())
            ->assertResponseStatus(200);      
    }

    //Check storeTeam function when it not work
    public function testStoreTeamFalse()
    {
        $response = $this -> post('/api/teams', []);
        $response->assertResponseStatus(422);            
    }

    //Check table is where are current user
    public function testIfDataExistsInTeamsDatabase()
    {
        $team = Team::factory()->create();

        $this->seeInDatabase('teams', [
            'title' => $team->title
        ]);
    }

    //Check updateTeam function
    public function testUpdateTeam()
    {
        $team = Team::factory()->create();

        $response = $this->put('/api/teams/' . $team->id, [
            'title' => 'Team_example_update'
        ]);        
        $response->assertTrue(true);        
    }

    // public function testIfUpdatedDataExistsInTeamsDatabase()
    // {
    //     $this->seeInDatabase('teams', [
    //         'title' => 'Team_example_update'
    //     ]);
    // }

    //Check updateTeam function when there is no specified user
    public function testUpdateTeamFalse()
    {
        $response = $this->put('/api/teams/10000000000000', []);
        $response ->assertTrue(true);
    }

    //Check deletTeam function
    public function testDeletTeam()
    {
       $team = Team::factory()->create();
       $team = Team::latest()->first();

        if($team){
            $team->delete();
        }

        $this->assertTrue(true);
    }

    //Check deletTeam function when there is no specified user
    public function testDeletTeamFalse()
    {
        $response = $this->delete('/api/teams/abc', []);
        $response->assertResponseStatus(400);
    }

    
    public function testTeamCannotCreateTaskForOtherTeam()
    {
        $team = Team::factory()->create();
        $team2 = Team::factory()->create();      

        $this->assertNotEquals($team2->id,$team->id);       
    }

    //Check if team exists in database
    public function testTeamDuplication()
    {
        $team1 = Team::make([
            'title' => 'Team_example_2'
        ]);

        $team2 = Team::make([
            'title' => 'Team_example_3'
        ]);

        $this->assertTrue($team1->title != $team2->title);
    }

}
