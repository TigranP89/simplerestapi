<?php

use App\Models\Role;
use App\Models\Team;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;


class RoleControllerTest extends TestCase
{

    //Check RoleController index function. Show all teams
    public function testIndexRole()
    {
        $response = $this -> get('/api/roles');
        $response->seeJsonStructure(['0']);
        $response->assertResponseStatus(200);
    }

    //Check showRole function
    public function testshowRole()
    {
        $role = Role::factory()->create();
        
        $response = $this->get('/api/roles/' . $role->id, []);
        $response->assertResponseStatus(200);
    }

    //Check showRole function when there is no specified team
    public function testShowRoleFalse()
    {
        $response = $this->get('/api/roles/10000000000000', []);
        $response->assertResponseStatus(400);
    }

    //Check storeTeam function
    public function testStoreTeam()
    {
        // //Method 1

        // $response = $this->post('/api/roles', [
        //     'title' => 'Team_example'
        //     'title' => 'Team_example'
        //     'title' => 'Team_example'
        // ]);

        // $response ->assertTrue(true);

        //Method 2

        $role = Role::factory()->create();

        $this->json('post', 
            "/api/roles", $role->toArray())
            ->assertResponseStatus(200);      
    }

    //Check storeTeam function when it not work
    public function testStoreTeamFalse()
    {
        $response = $this -> post('/api/roles', []);
        $response->assertResponseStatus(422);            
    }

    // //Check table is where are current user
    // public function testIfDataExistsInRolesDatabase()
    // {
    //     $role = Role::factory()->create();

    //     $this->seeInDatabase('users_role', [
    //         'title' => $team->title
    //     ]);
    // }

    //Check updateRole function
    public function testUpdateRole()
    {
        $role = Role::factory()->create();

        $response = $this->put('/api/roles/' . $role->id, [
            'user_id' => '2',
            'team_id' => '3',
            'role' => 'Ex_Program'
        ]);        
        $response->assertTrue(true);        
    }

    public function testIfUpdatedDataExistsInRolesDatabase()
    {
        $this->seeInDatabase('users_role', [
            'user_id' => '2',
            'team_id' => '3',
            'role' => 'Ex_Program'
        ]);
    }

    //Check updateRole function when there is no specified user
    public function testUpdateRoleFalse()
    {
        $response = $this->put('/api/roles/10000000000000', []);
        $response ->assertTrue(true);
    }

    //Check deletRole function
    public function testDeletRole()
    {
       $role = Role::factory()->create();
       $role = Role::latest()->first();

        if($role){
            $role->delete();
        }

        $this->assertTrue(true);
    }

    //Check deletRole function when there is no specified user
    public function testDeletRoleFalse()
    {
        $response = $this->delete('/api/roles/abc', []);
        $response->assertResponseStatus(400);
    }

    
    public function testRoleCannotCreateTaskForOtherRole()
    {
        $role = Role::factory()->create();
        $role2 = Role::factory()->create();      

        $this->assertNotEquals($role2->id,$role->id);       
    }

    // //Check if team exists in database
    // public function testTeamDuplication()
    // {
    //     $team1 = Team::make([
    //         'title' => 'Team_example_2'
    //     ]);

    //     $team2 = Team::make([
    //         'title' => 'Team_example_3'
    //     ]);

    //     $this->assertTrue($team1->title != $team2->title);
    // }


    //Check getUserTeams function
    public function testgetUserTeams()
    {
        $role = Role::factory()->create();
        
        $response = $this->get('/api/roles/user/' . $role->user_id, []);
        $response->assertResponseStatus(200);
    }       
    
    //Check getUserTeams function when there is no specified user
    public function testgetUserTeamsFalse()
    {
        $response = $this->get('/api/roles/user/10000000000000', []);
        $response->assertResponseStatus(400);
    }
    

}
