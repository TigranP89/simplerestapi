<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;


class UserControllerTest extends TestCase
{
    public function testSomeFun()
    {
        $response = $this->call('GET', '/');

        $this->assertEquals(200, $response->status());
    }

    public function testSomeFun2()
    {
        $response = $this->call('POST', '/api/users', ['name' => 'Taylor']);

        $this->assertEquals(422, $response->status());
    }

    public function testDatabase()
    {
        $this->seeInDatabase('users', ['email' => 'sally@sally.com']);
    }

    //Check index function. Show all users
    public function testIndex()
    {
        $response = $this -> get('/api/users');
        $response->seeJsonStructure(['0']);
        $response->assertResponseStatus(200);
    }

    //Check showUser function
    public function testShowUser()
    {
        $user = User::factory()->create();

        $response = $this->get('/api/users/' . $user->id, []);
        $response->assertResponseStatus(200);
    }

    //Check showUser function when there is no specified user
    public function testShowUserFalse()
    {
        $response = $this->get('/api/users/10000000000000', []);
        $response->assertResponseStatus(404);
    }

    //Check storeUser function
    public function testStoreUser()
    {
        //Method 1

        $response = $this->post('/api/users', [
            'name' => 'Sally',
            'email' => 'sally@sally.com'
        ]);

        $response ->assertTrue(true);

        //Method 2

        $user = User::factory()->create();

        $this->json('post', 
            "/api/users", $user->toArray())
            ->assertResponseStatus(422);      
    }

    //Check storeUser function when it not work
    public function testStoreUserFalse()
    {
        $response = $this -> post('/api/users', []);
        $response->assertResponseStatus(422);            
    }

    //Check table is where are current user
    public function testIfDataExistsInDatabase()
    {
        $user = User::factory()->create();

        $this->seeInDatabase('users', [
            'name' => $user->name
        ]);
    }

    //Check updateUser function
    public function testUpdateUser()
    {
        $user = User::factory()->create();

        $response = $this->put('/api/users/' . $user->id, [
            'name' => 'Sally-1',
            'email' => 'sally@sally.com'
        ]);        
        $response->assertTrue(true);        
    }

    public function testIfUpdatedDataExistsInDatabase()
    {
        $this->seeInDatabase('users', [
            'name' => 'Sally-1',
            'email' => 'sally@sally.com'
        ]);
    }

    //Check updateUser function when there is no specified user
    public function testUpdateUserFalse()
    {
        $response = $this->put('/api/users/10000000000000', []);
        $response ->assertTrue(true);
    }

    //Check deletUser function
    public function testDeletUser()
    {
       $user = User::factory()->create();
       $user = User::latest()->first();

        if($user){
            $user->delete();
        }

        $this->assertTrue(true);
    }

    //Check deletUser function when there is no specified user
    public function testDeletUserFalse()
    {
        $response = $this->delete('/api/users/abc', []);
        $response->assertResponseStatus(400);
    }

    
    public function testUserCannotCreateTaskForOtherUser()
    {
        $user = User::factory()->create();
        $user2 = User::factory()->create();      

        $this->assertNotEquals($user2->id,$user->id);       
    }

    //Check if user exists in database
    public function testUserDuplication()
    {
        $user1 = User::make([
            'name' => 'John Doe',
            'email' => 'johndoe@gmail.com'
        ]);

        $user2 = User::make([
            'name' => 'Mary Jane',
            'email' => 'maryjane@gmail.com'
        ]);

        $this->assertTrue($user1->name != $user2->name);
    }

}
