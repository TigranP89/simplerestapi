<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();    
});

$router->get('/key', function() {
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle(str_repeat($pool, 5)), 0, 32);
});

$router->group(['prefix' => 'api'], function($router){
    $router->get('/users', 'UserController@index');
    $router->get('/users/{id}', 'UserController@showUser');
    $router->post('/users', 'UserController@storeUser');
    $router->put('/users/{id}', 'UserController@updateUser');
    $router->delete('/users/{id}', 'UserController@deletUser');
    
    $router->get('/teams', 'TeamController@index');
    $router->get('/teams/{id}', 'TeamController@showTeam');
    $router->post('/teams', 'TeamController@storeTeam');
    $router->put('/teams/{id}', 'TeamController@updateTeam');
    $router->delete('/teams/{id}', 'TeamController@deletTeam');

    $router->get('/roles', 'RoleController@index');
    $router->get('/roles/{id}', 'RoleController@showRole');
    $router->post('/roles', 'RoleController@storeRole');
    $router->put('/roles/{id}', 'RoleController@updateRole');
    $router->delete('/roles/{id}', 'RoleController@deletRole');

    $router->get('/roles/user/{id}', 'RoleController@getUserTeams');
});