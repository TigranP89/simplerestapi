<?php

namespace App\Http\Controllers;

use App\Models\Team;
// use Illuminate\Http\Client\Request;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        // return response()->json(app('db')->select("SELECT * FROM teams"));
        return response()->json(Team::all());
    }

    public function showTeam($id)
    {
        // $show_id = app('db')->select("SELECT id FROM teams WHERE id = '$id'");
        $show_id = Team::select('id')->where('id', $id);
        if(!empty($show_id)){
            return response()->json(Team::findOrFail($id));
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }

    public function storeTeam(Request $request)
    {   
        $this->validate($request, [
            'title' => 'required'
        ]);

        $date = date('Y-m-d H:i:s');

        // $team = app('db')->insert("INSERT INTO teams (title, created_at, updated_at) VALUES ('$request->title', '$date', '$date')");

        $team = new Team();
        $team->title = $request->title;       
        // $team->created_at = $date;
        // $team->updated_at = $date;
        $team->save();
        return response()->json($team);
    }

    public function updateTeam(Request $request, $id)
    {   
        $this->validate($request, [
            'title' => 'required'
        ]);

        // $update_id = app('db')->select("SELECT id FROM teams WHERE id = '$id'");
        $update_id = Team::select('id')->where('id', $id);
        $date = date('Y-m-d H:i:s');

        if(!empty($update_id)){
            // $team = app('db')->update("UPDATE teams SET title = '$request->title',  updated_at = '$date' WHERE id = '$id'");
            $team = Team::findOrFail($id);
            $team->title = $request->title;            
            $team->created_at = $date;
            $team->updated_at = $date;
            $team->update();

            return response()->json($team, 200);
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }

    public function deletTeam($id)
    {
        // $delete_id = app('db')->select("SELECT id FROM teams WHERE id = '$id'");
        $delete_id = Team::where('id', $id)->first();

        if(!empty($delete_id)){
            // return response()->json(app('db')->delete("DELETE FROM teams WHERE id = '$id'"));
            $team = Team::findOrFail($id);
            $team->delete();
            return response()->json($team, 200);
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }
}
