<?php

namespace App\Http\Controllers;

use App\Models\User;
// use Illuminate\Http\Client\Request;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {           
        return response()->json(User::all());
    }

    public function showUser($id)
    {
        // $show_id = app('db')->select("SELECT id FROM users WHERE id = '$id'");
        $show_id = User::select('id')->where('id', $id);
       
        if(!empty($show_id)){
            return response()->json(User::findOrFail($id));
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }

    public function storeUser(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);

        $date = date('Y-m-d H:i:s');

        // $user = app('db')->insert("INSERT INTO users (name, email, created_at, updated_at) VALUES ('$request->name', '$request->email', '$date', '$date')");
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        // $user->created_at = $date;
        // $user->updated_at = $date;
        $user->save();
       
        return response()->json($user, 201);
    }

    public function updateUser(Request $request, $id)
    {    
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);
                   
        // $update_id = app('db')->select("SELECT id FROM users WHERE id = '$id'");
        $update_id = User::select('id')->where('id', $id);
        $date = date('Y-m-d H:i:s');
        
        if(!empty($update_id)){
            // $user = app('db')->update("UPDATE users SET name = '$request->name', email = '$request->email',  updated_at = '$date' WHERE id = '$id'");
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->created_at = $date;
            $user->updated_at = $date;
            $user->update();

            return response()->json($user, 200);
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }

    public function deletUser($id)
    {
        
        // $delete_id = app('db')->select("SELECT id FROM users WHERE id = '$id'");
        $delete_id = User::where('id', $id)->first();

        if(!empty($delete_id)){
            // return response()->json(app('db')->delete("DELETE FROM users WHERE id = '$id'"));
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json($user, 200);
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }
}
