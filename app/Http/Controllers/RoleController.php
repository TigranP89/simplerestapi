<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Team;
use App\Models\User;
// use Illuminate\Http\Client\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        // return response()->json(app('db')->select("SELECT * FROM users_role"));        
        return response()->json(Role::all());
    }

    public function showRole($id)
    {
        
        // $show_id = app('db')->select("SELECT id FROM users_role WHERE id = '$id'");
        $show_id = Role::where('id', $id)->first();
        
        if(!empty($show_id)){
            return response()->json(Role::findOrFail($id));
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }

    public function storeRole(Request $request)
    {   
        $this->validate($request, [
            'user_id' => 'required',
            'team_id' => 'required',
            'role' => 'required'
        ]);
        
        // $user_id = app('db')->select("SELECT id FROM users WHERE id = '$request->user_id'");
        // $team_id = app('db')->select("SELECT id FROM teams WHERE id = '$request->team_id'");
        $user_id = User::select('id')->where('id', $request->user_id);
        $team_id = Team::select('id')->where('id', $request->team_id);
        $date = date('Y-m-d H:i:s');

        if( !empty($user_id) && !empty($team_id)){
            // $role = app('db')->insert("INSERT INTO users_role (user_id, team_id, role, created_at, updated_at) VALUES ('$request->user_id', '$request->team_id',  '$request->role','$date', '$date')");
            $role = new Role();
            $role->user_id = $request->user_id;
            $role->team_id = $request->team_id;
            $role->role = $request->role;
            $role->save();
            return response()->json($role);
        } else {
            return response()->json("No such user or team", $status = 400);            
        }
    }

    public function updateRole(Request $request, $id)
    {   
        $this->validate($request, [
            'user_id' => 'required',
            'team_id' => 'required',
            'role' => 'required'
        ]);

        
        // $update_id = app('db')->select("SELECT id FROM users_role WHERE id = '$id'");
        // $user_id = app('db')->select("SELECT id FROM users WHERE id = '$request->user_id'");
        // $team_id = app('db')->select("SELECT id FROM teams WHERE id = '$request->team_id'");
        $update_id = Role::where('id', $id);
        $user_id = User::where('id', $request->user_id);
        $team_id = Team::where('id', $request->team_id);

        $date = date('Y-m-d H:i:s');

        if(!empty($update_id)){
            if( !empty($user_id) && !empty($team_id)){
                // $role = app('db')->update("UPDATE users_role SET user_id = '$request->user_id', team_id = '$request->team_id', role = '$request->role',  updated_at = '$date' WHERE id = '$id'");
                $role = Role::findOrFail($id);
                $role->user_id = $request->user_id;
                $role->team_id = $request->team_id;
                $role->role = $request->role;
                $role->update();

                return response()->json($role, 200);
            } else {
                return response()->json("No such user or team", $status = 400);            
            }
        } else {
            return response()->json("No such id", $status = 400);
        }                
    }

    public function deletRole($id)
    {
        // $delete_id = app('db')->select("SELECT id FROM users_role WHERE id = '$id'");
        $delete_id = Role::where('id', $id)->first();
        if(!empty($delete_id)){
            // return response()->json(app('db')->delete("DELETE FROM users_role WHERE id = '$id'"));
            $team = Role::findOrFail($id);
            $team->delete();
            return response()->json($team, 200);
        } else {
            return response()->json("No such id", $status = 400);
        }        
    }
    
    public function getUserTeams($id)
    {
        $team_array = [];
        // $get_user_id = DB::select("SELECT * FROM users WHERE id = ". $id);

        $get_user_id = User::where('id', $id)->first();               
        
        if(!empty($get_user_id->id)){
            // $get_teams_id = DB::select("SELECT team_id FROM users_role WHERE user_id = ". $get_user_id->id);
            
            // foreach($get_teams_id as $team_id){
            //     $user_teams = DB::select("SELECT * FROM teams WHERE id =". $team_id->team_id);    
            //     array_push($team_array, $user_teams);          
            // }

            // return response()->json($team_array, $status = 200);

            $get_user_rols = Role::where('user_id', $id)->get();
            foreach($get_user_rols as $get_teams_id){           
                $user_teams =Team::where('id', $get_teams_id->team_id)->get();            
                foreach($user_teams as $user_team){
                    array_push($team_array, [
                        'title' => $user_team->title ?? '',
                        'created_at' => $user_team->created_at ?? '',
                        'updated_at' => $user_team->updated_at ?? '',                        
                    ]);
                }            
            }
            return response()->json($team_array, $status = 200);            
        } else {
            return response()->json("No such user", $status = 400);
        }
    }
}
